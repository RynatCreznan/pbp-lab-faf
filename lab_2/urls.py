from lab_2.views import index,xml,json
from django.urls import path
# from .views import

urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json'),
    
]
