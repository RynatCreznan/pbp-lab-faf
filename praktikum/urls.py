from django.urls import path, re_path, include
from django.contrib import admin
import lab_1.urls as lab_1
from lab_1.views import index as index_lab1

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lab-1/', include('lab_1.urls')),
    path('lab-2/', include('lab_2.urls')),
    path('lab-3/', include('lab_3.urls')),
    path('lab-4/', include('lab_4.urls')),
    re_path(r'^$', index_lab1, name='index')
]
